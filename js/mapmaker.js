/*
    This is a FRONT END JS FILE

    This file assumes the existence of a div with id "mapdiv"

    This script will handle
        Creating and Populating the map on the frontend using the given array
 */

/*
    Function takes an ARRAY of JSON objects of the form { name, geo: {latitude, longitude} }
 */
function mapinit(places, options=null) {

    if (places) {
        //setup map
        var mymap = L.map('mapdiv');
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoiYmVub3oxMSIsImEiOiJjazNpMmsyeGIwM3ZnM2JwaW9mdG9sdWl1In0.RrwSfVxBLJhqSK3aTsEaNw'
        }).addTo(mymap);

        mymap.setView([places[0].geo.latitude, places[0].geo.longitude], 6); //manually setting initial view to the first geopoint on map

        //marker layer group
        var markerLayer = L.layerGroup()

        //pin markers
        for (place of places) {
            var startDate = (place.startDate === undefined) ? undefined : place.startDate.toString() //setting time variable to the startDate if exists, set undefined to 0000 to aid in sorting
            var endDate = (place.endDate === undefined) ? undefined : place.endDate.toString()
            var marker = L.marker([place.geo.latitude, place.geo.longitude], {color: 'red', startDate: startDate, timeStrLength: 4, alwaysShowDate: true, endDate: endDate})
            marker.bindPopup(place.name + "<br><br>latitude: " + place.geo.latitude + "<br>longitude: " + place.geo.longitude 
                + "<br>startDate: " + place.startDate + "<br>endDate: " + place.endDate + "<br>"
                + "<br><a href='" + "'>go to item</a>")
            marker.addTo(markerLayer)
        }

        //time slider
        var sliderControl = L.control.sliderControl({position: "topright", layer: markerLayer, timeStrLength: 4, timeAttribute: 'startDate', range: true, clusters: true}); //calls the initialize function
        mymap.addControl(sliderControl); //Adds the slider to the map, calls the onAdd function
        sliderControl.startSlider(); //calls the startSlider function
    }
};