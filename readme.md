# Enhanced Slider Controls for leaflet

### Basic Usage
Current usage involes including the relevant files in your html, where ${lib} is the relative path to the html file

```
<link rel='stylesheet' href='${lib}/css/leaflet.css'/>
<script src='${lib}/js/leaflet.js'></script>
<script src='${lib}/js/BenSliderControl.js'></script>
<link rel='stylesheet' href='${lib}/css/jquery-ui.css'/>
<script src='${lib}/js/jquery-ui.js'></script>
<link rel='stylesheet' href='${lib}/css/benmap.css'/>

<link rel='stylesheet' href='${lib}/css/MarkerCluster.css'/>
<link rel='stylesheet' href='${lib}/css/MarkerCluster.Default.css'/>
<script src='${lib}/js/leaflet.markercluster.js'></script>

<div id='mapdiv' style='height: 420px;'></div>
<script src='${lib}/js/mapmaker.js'></script><!-- Handles a lot of the marker stuff -->
<script>mapinit(${JSON.stringify(places)})</script>
```

where "places" is an array of json objects of the format:

```
{
	id,
	name,
	geo: {latitude,longitude},
	description,
	startDate,
	endDate
}
```

startDate, endDate can be undefined

if startDate is undefined the item will be shown under the "show items without dates" tickbox on the slider control

### options

options are currently hardcoded in mapmaker.js , most importantly "timeAttribute" "range" and "clusters" in the sliderControl object on line 44:

```
var sliderControl = L.control.sliderControl({position: "topright", layer: markerLayer, timeStrLength: 4, timeAttribute: 'startDate', range: true, clusters: true});
```

timeAttribute is the marker var that contains the date which we want to sort by.

range: true means that items are active for a period of time (eg 1850 to 1860) instead of a single date, range: false means the items are just a single date

clusters: true means that we will use leaflet.markercluster addon to group nearby markers into clusters for a much cleaner look.


### About
Made by Benjamin McDonnell for UTS under the TLCMap project
